//WAP to find the sum of n fractions.
#include<stdio.h>
#include<math.h>
typedef struct point
{
	float x;
	float y;
}point;
typedef struct rectangle
{
	point a,b,c;
	float ab,bc,ac;
	float area;
}rectangle;
float distance(point a,point b)
{
	float dist=sqrt(pow((a.x-b.x),2)+pow((a.y-b.y),2));
	return dist;
}
point input_point()
{
	point p;
	printf("Enter the abscissa(x-coordinate):");
	scanf("%f",&p.x);
	printf("Enter the ordinate(y-coordinate):");
	scanf("%f",&p.y);
	return p;
}
rectangle input_rectangle()
{
rectangle r;
printf("Enter the first point\n");
r.a=input_point();
printf("Enter the second point\n");
r.b=input_point();
printf("Enter the third point\n");
r.c=input_point();
return r;
}
void input_n_rectangles(int n, rectangle a[n])
{
for(int i=0; i<n; i++)
{
	printf("Enter the vertices for %d rectangle\n",i+1);
	a[i]=input_rectangle();
}
}
float min(float a, float b, float c)
{
	if(a<=b)
		if(a<=c)
			return a;
		else if(c<a)
			return c;
	else if(b<a)
		if(c<=b)
			return c;
	else if(a<=c)
			return b;
}
float max(float a, float b, float c)
{
	if(a<=b)
		if(a<=c)
			return a;
		else if(c<a)
			return c;
	else if(b<a)
		if(c<=b)
			return c;
		else if(a<=c)
			return b;
}
void compute_area_of_1_rectangle(rectangle *r)
{
r->ab=distance(r->a,r->b);
r->bc=distance(r->b,r->c);
r->ac=distance(r->a,r->c);
float side1=min(r->ab,r->bc,r->ac);
float side3=max(r->ab,r->bc,r->ac);
float sum=r->ab+r->bc+r->ac;
float side2=sum-side1-side3;
r->area=side1*side2;
}
void compute_area_of_n_rectangles(int n, rectangle a[n])
{
	for(int i=0;i<n;i++)
		compute_area_of_1_rectangle(&a[i]);
}
void display_area(rectangle r)
{
	printf("Area of rectangle with vertices :(%.2f,%.2f),(%.2f,%.2f),(%.2f,%.2f) is %.2f\n",r.a.x,r.a.y,r.b.x,r.b.y,r.c.x,r.c.y,r.area);
}
void display_area_of_n_rectangles(int n, rectangle a[n])
{
	for(int i=0; i<n;i++)
	{
		display_area(a[i]);
	}
}
int input()
{
int n;
	printf("Enter the number of rectangles:");
	scanf("%d",&n);
return n;
}
int main()
{
	int n=input();
	rectangle a[n];
	input_n_rectangles(n,a);
	compute_area_of_n_rectangles(n,a);
	display_area_of_n_rectangles(n,a);
	return 0;
}